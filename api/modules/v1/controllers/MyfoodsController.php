<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\data\ActiveDataProvider;

class MyfoodsController extends \api\common\controllers\ActiveController
{
    public $modelClass = 'api\modules\v1\models\Food';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => \sizeg\jwt\JwtHttpBearerAuth::class,
            'except' => ['options'],
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['create'], $actions['update'], $actions['delete'], $actions['view']);

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $userId = Yii::$app->user->id;

        if (isset($userId)) {
            return new ActiveDataProvider([
                'query' => $this->modelClass::find()->andWhere(['created_by' => $userId]),
                'pagination' => [
                    'class' => 'yii\data\Pagination',
                    'validatePage' => false,
                ],
            ]);
        }

        throw new \yii\web\ForbiddenHttpException(sprintf('Debe estar registrado para realizar esta acción'));
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        // check if the user can access $action and $model
        // throw ForbiddenHttpException if access should be denied
        if ('update' === $action || 'delete' === $action) {
            if ($model->created_by !== \Yii::$app->user->id) {
                throw new \yii\web\ForbiddenHttpException(sprintf('No tienes permiso para realizar esta acción'));
            }
        }
    }
}
