<?php

namespace api\modules\v1\models;

class Comment extends \common\models\Comment
{
    public function fields()
    {
        return ['id', 'text', 'start', 'createdBy', 'updated_at'];
    }

    public function extraFields()
    {
        return ['food', 'created_at'];
    }

    public function getFood()
    {
        return $this->hasOne(Food::className(), ['id' => 'post_id']);
    }

    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
}
