<?php

namespace api\modules\v1\models;

class User extends \api\common\models\User
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = parent::rules();

        array_push(
            $rules,
            [['username', 'email'], 'required'],
        );

        return $rules;
    }

    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        return [
            'id',
            'name' => function ($model) {
                return $model->profile ?
                $model->profile->name : null;
            },
            'profile_picture' => function ($model) {
                return $model->profile ?
                $model->profile->profile_picture : null;
            },
            'email',
            'phone_number' => function ($model) {
                return $model->profile ?
                $model->profile->phone : null;
            },
            'movil_number' => function ($model) {
                return $model->profile ?
                $model->profile->mobile : null;
            },
            'provincia' => function ($model) {
                return $model->profile ?
                $model->profile->location : null;
            },
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function extraFields()
    {
        return [
            'username',
            'is_provider' => function ($model) {
                return $model->profile ?
                $model->profile->is_provider : null;
            },
            'created_at',
            'updated_at',
            'profile',
            'cant_foods',
        ];
    }

    public function getFoods()
    {
        return $this
            ->hasMany(Food::class, ['id' => 'post_id'])
            ->viaTable('user_post', ['user_id' => 'id'])
        ;
    }

    public function getCant_foods()
    {
        return (int) $this->hasMany(Food::class, ['created_by' => 'id'])->count();
    }
}
