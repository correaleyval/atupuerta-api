<?php

namespace api\modules\v1\models;

use Exception;
use Yii;

class Food extends \common\models\Post
{
    public $post_validation;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = parent::rules();

        array_push(
            $rules,
            ['category', 'default', 'value' => 'food'],
            [['active', 'post_validation'], 'boolean'],
            [['active', 'post_validation'], 'default', 'value' => false],
            [['image1', 'image2', 'image3'], 'string', 'max' => 255],
            ['post_validation', function ($attribute, $params) {
                $userId = isset(Yii::$app->user->id)
                ? Yii::$app->user->id
                : null;

                $user = User::findOne($userId);

                if (!$user) {
                    $this->addError($attribute, 'El usuario no tiene permisos para realizar esta acción');
                }

                if (!$user->profile) {
                    $this->addError($attribute, 'El usuario debe completar el registro para realizar esta acción');
                }

                if (!$user->profile->phone && !$user->profile->mobile) {
                    $this->addError($attribute, 'El usuario debe registrar al menos un número de teléfono para realizar esta acción');
                }

                $cant_posts_by_user = $user->getCant_foods();

                if ($cant_posts_by_user >= 3) {
                    $this->addError($attribute, 'El usuario ha excedido el número de publicaciones permitidas');
                }
            }]
        );

        return $rules;
    }

    public static function find()
    {
        $query = parent::find();

        return $query->andWhere(['category' => 'food']);
    }

    public function fields()
    {
        return ['id', 'title', 'description', 'price', 'moneyType', 'updated_at', 'image1', 'is_favorite', 'cant_comments', 'star'];
    }

    public function extraFields()
    {
        return ['created_at', 'category', 'comments', 'createdBy', 'image2', 'image3', 'active'];
    }

    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['post_id' => 'id']);
    }

    public function getCant_comments()
    {
        return $this->getComments()->count();
    }

    public function getStar()
    {
        return (int) $this->getComments()->average('start');
    }

    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getIs_favorite()
    {
        $user = User::findOne(Yii::$app->user->id);

        if (!isset($user)) {
            return false;
        }

        return $user->getPosts()->where(['id' => $this->id])->exists();
    }

    public function beforeValidate()
    {
        if (preg_match(
            '/^data:image\\/(?<extension>(?:png|gif|jpg|jpeg));base64,(?<image>.+)$/',
            $this->image1
        )) {
            $imagePath = 'foods_pictures/'.Yii::$app->security->generateRandomString();

            $image64 = $this->image1;

            $fileSaved = $this->base64ToImage($image64, $imagePath);

            $this->image1 = $fileSaved;
        }

        if (preg_match(
            '/^data:image\\/(?<extension>(?:png|gif|jpg|jpeg));base64,(?<image>.+)$/',
            $this->image2
        )) {
            $imagePath = 'foods_pictures/'.Yii::$app->security->generateRandomString();

            $image64 = $this->image2;

            $fileSaved = $this->base64ToImage($image64, $imagePath);

            $this->image2 = $fileSaved;
        }

        if (preg_match(
            '/^data:image\\/(?<extension>(?:png|gif|jpg|jpeg));base64,(?<image>.+)$/',
            $this->image3
        )) {
            $imagePath = 'foods_pictures/'.Yii::$app->security->generateRandomString();

            $image64 = $this->image3;

            $fileSaved = $this->base64ToImage($image64, $imagePath);

            $this->image3 = $fileSaved;
        }

        return parent::beforeValidate();
    }

    private static function base64ToImage($base64_string, $output_file)
    {
        try {
            if (
            preg_match(
                '/^data:image\\/(?<extension>(?:png|gif|jpg|jpeg));base64,(?<image>.+)$/',
                $base64_string,
                $matchings
            )
        ) {
                $imageData = base64_decode($matchings['image']);
                $extension = $matchings['extension'];
                $output_file = sprintf('%s.%s', $output_file, $extension);

                $file = fopen($output_file, 'wb');
                fwrite($file, $imageData);
                fclose($file);

                return $output_file;
            }
        } catch (Exception $err) {
            throw $err;
        }
    }
}
