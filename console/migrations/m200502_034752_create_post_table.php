<?php

namespace console\migrations;

/**
 * Handles the creation of table `{{%post}}`.
 */
class m200502_034752_create_post_table extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%post}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'image1' => $this->string(),
            'image2' => $this->string(),
            'image3' => $this->string(),
            'price' => $this->double(),
            'moneyType' => $this->string(),
            'description' => $this->text()->notNull(),
            'category' => $this->string(),
            'active' => $this->boolean()->defaultValue(true),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%post}}');
    }
}
