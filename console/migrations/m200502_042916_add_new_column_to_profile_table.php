<?php

namespace console\migrations;

/**
 * Handles adding columns to table `{{%profile}}`.
 */
class m200502_042916_add_new_column_to_profile_table extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile}}', 'profile_picture', $this->string()->null());
        $this->addColumn('{{%profile}}', 'phone', $this->string()->null());
        $this->addColumn('{{%profile}}', 'mobile', $this->string()->null());
        $this->addColumn('{{%profile}}', 'is_provider', $this->boolean()->notNull()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profile}}', 'profile_picture');
        $this->dropColumn('{{%profile}}', 'phone');
        $this->dropColumn('{{%profile}}', 'mobile');
        $this->dropColumn('{{%profile}}', 'is_provider');
    }
}
