<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$categoryOptions = \common\models\Post::find()->select('category')->column();
$categoryOptions = array_combine($categoryOptions, $categoryOptions);

$moneyTypeOptions = \common\models\Post::find()->select('moneyType')->column();
$moneyTypeOptions = array_combine($moneyTypeOptions, $moneyTypeOptions);

$this->title = Yii::t('app', 'Posts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'active:boolean',
            [
                'attribute' => 'category',
                'filter' => $categoryOptions,
            ],
            'title',
            'price:currency',
            [
                'attribute' => 'moneyType',
                'filter' => $moneyTypeOptions
            ],
            'description:ntext',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-block btn-success']),
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
